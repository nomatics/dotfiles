;;; init.el -- My Emacs configuration
;-*-Emacs-Lisp-*-


(require 'package)
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/"))
(package-initialize)

(unless (package-installed-p 'use-package)
    (package-refresh-contents)
      (package-install 'use-package))

(eval-when-compile
    (require 'use-package))

;; install packages
(use-package evil :ensure t )


;; evil
(evil-mode)
(evil-set-initial-state 'term-mode 'emacs)
