;;
;; Essential settings.

(setq inhibit-splash-screen t)
(setq inhibit-startup-message t)
(setq inhibit-startup-echo-area-message t)
(setq visual-line-fringe-indicators '(left-curly-arrow right-curly-arrow))
(global-eldoc-mode -1)
(setq-default indicate-empty-lines nil)
(setq-default indent-tabs-mode nil) (defalias 'yes-or-no-p 'y-or-n-p)
(setq vc-follow-symlinks t)
(setq large-file-warning-threshold nil)
(setq split-width-threshold nil)
(setq custom-safe-themes t)
(column-number-mode t)
(add-to-list 'custom-theme-load-path (expand-file-name "themes" user-emacs-directory))
(setq ring-bell-function 'ignore)

(setq redisplay-dont-pause t)
(setq scroll-margin 1)
(setq scroll-step 1)
(setq scroll-conservatively 100000)
(setq scroll-preserve-screen-position 1)

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(setq initial-scratch-message nil)
;; PACKAGES
(package-initialize)
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/")) (require 'package)
(unless (package-installed-p 'use-package) (package-refresh-contents) (package-install 'use-package))
(setq use-package-always-ensure t)
;;
;;
;; THEME
;;
(add-hook 'prog-mode-hook #'display-line-numbers-mode)
(menu-bar-mode -1)
(tool-bar-mode -1)
(add-to-list 'default-frame-alist '(height . 53))
(add-to-list 'default-frame-alist '(width . 185))

(use-package monokai-theme :ensure t)
(load-theme 'monokai t)

(use-package powerline :ensure t)
(powerline-default-theme)
;;
;;
(use-package evil)
(evil-mode)
(evil-set-initial-state 'term-mode 'emacs)

(use-package which-key)
(which-key-mode)

(use-package powerline-evil )
(use-package undo-tree )
(use-package restart-emacs)
;;(setq restart-emacs-restore-frames t)
(use-package projectile)

;; HELM
(use-package helm :ensure t)
(helm-mode 1)
(setq helm-echo-input-in-header-line t)

(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to do persistent action
(define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB works in terminal
(define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z
(add-hook 'helm-minibuffer-set-up-hook 'spacemacs//helm-hide-minibuffer-maybe)

(defun spacemacs//helm-hide-minibuffer-maybe ()
  "Hide minibuffer in Helm session if we use the header line as input field."
  (when (with-helm-buffer helm-echo-input-in-header-line)
    (let ((ov (make-overlay (point-min) (point-max) nil nil t)))
      (overlay-put ov 'window (selected-window))
      (overlay-put ov 'face
                   (let ((bg-color (face-background 'default nil)))
                     `(:background ,bg-color :foreground ,bg-color)))
      (setq-local cursor-type nil))))


(use-package helm-projectile)
(use-package helm-ag)
(use-package helm-swoop)
(helm-projectile-on)
(use-package pcre2el)
 (defun spacemacs/helm-project-do-ag-region-or-symbol ()
        "Search in current project with `ag' using a default input."
        (interactive)
        (let ((dir (projectile-project-root)))
          (if dir
              (spacemacs//helm-do-ag-region-or-symbol 'helm-do-ag dir)
            (message "error: Not in a project."))))

(defun spacemacs//helm-do-ag-region-or-symbol (func &optional dir)
        "Search with `ag' with a default input."
        (require 'helm-ag)
        (cl-letf* (((symbol-value 'helm-ag-insert-at-point) 'symbol)
                   ;; make thing-at-point choosing the active region first
                   ((symbol-function 'this-fn) (symbol-function 'thing-at-point))
                   ((symbol-function 'thing-at-point)
                    (lambda (thing)
                      (let ((res (if (region-active-p)
                                     (buffer-substring-no-properties
                                      (region-beginning) (region-end))
                                   (this-fn thing))))
                        (when res (rxt-quote-pcre res))))))
          (funcall func dir)))




;; Keybinds
(defun reload-config () (interactive) (load-file "~/.emacs"))
(use-package general)


(defun spacemacs/helm-swoop-region-or-symbol ()
        "Call `helm-swoop' with default input."
        (interactive)
        (let ((helm-swoop-pre-input-function
               (lambda ()
                 (if (region-active-p)
                     (buffer-substring-no-properties (region-beginning)
                                                     (region-end))
                   (let ((thing (thing-at-point 'symbol t)))
                     (if thing thing ""))))))
          (call-interactively 'helm-swoop)))

(use-package all-the-icons)
(use-package neotree)
(setq neo-smart-open t)
(setq neo-theme (if (display-graphic-p) 'icons 'arrow))

(general-def
 :prefix "SPC"
 :keymaps 'normal
 "q"   '(nil :which-key "quit")
 "qr"  'restart-emacs
 "r"   'reload-config
 "x"   '(helm-M-x :which-key "extended-commands")
 "b"   '(nil :which-key "buffers")
 "bl"  '(helm-mini :wk "buffer-list" )
 "bk"  'kill-buffer
 "be"  '((lambda () (interactive) (find-file "~/.emacs")) :wk "emacs-config")
 "s"   '(nil :wk "search")
 "sa"  'helm-do-ag
 "sP"  'spacemacs/helm-project-do-ag-region-or-symbol
 "sp"  'helm-projectile-ag
 "sS"  'spacemacs/helm-swoop-region-or-symbol
 "ss"  'helm-swoop
 "h"   '(:keymap help-map :wk "help")
 "t"   '(nil :wk "toggle")
 "tn"   '(neotree-toggle :wk "toggle")
 "tt"   '(multi-term :wk "terminal")

 )

(general-def
  :keymaps 'neotree-mode-map
  :states 'normal
  "TAB" 'neotree-enter
  "RET" 'neotree-enter
  "q" 'neotree-hide
  "g" 'neotree-refresh
  "n" 'neotree-next-line
  "p" 'neotree-previous-line
  "A" 'neotree-stretch-toggle
  "H" 'neotree-hidden-file-toggle
  "d" 'neotree-delete-node
  )

(general-def
  :keymaps 'helm-map
  "<escape>" 'keyboard-escape-quit
  )


(use-package multi-term)
(setq multi-term-program "/bin/bash")
(setq term-unbind-key-list nil)
(setq term-bind-key-alist '(("ESC" . term-send-raw)))

(defun trim-linefeed-right (string)
  "Remove trailing linefeed from STRING."
  (if (string-match "[\n\r]+" string)
      (replace-match "" t t string)
    string))


(defun term-evil-past-after ()
  (interactive)
  (term-send-raw-string
   (trim-linefeed-right
    (evil-paste-after evil-paste-count))))
  

(defun term-evil-past-before ()
  (interactive)
  (term-send-raw-string (evil-paste-before evil-paste-count)))

(defun toggle-term-evil-line-char ()
  (interactive)
  (if (string= evil-state "emacs")
      (progn
	(evil-normal-state)
	(term-line-mode))
    (progn
      (evil-emacs-state)
      (term-char-mode))))

(defun toggle-term-evil ()
  (interactive)
  (if (string= evil-state "emacs")
      (evil-normal-state)
    (evil-emacs-state)))

(general-def
  :keymaps 'term-raw-map
  "C-j" 'toggle-term-evil-line-char
  )

(general-def
  :keymaps 'term-raw-map
  :states 'normal
  "p" 'term-evil-past-after
  "P" 'term-evil-past-before)

(general-def
  :keymaps 'normal
  "q" 'nil
  )

(use-package rainbow-delimiters)
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)

